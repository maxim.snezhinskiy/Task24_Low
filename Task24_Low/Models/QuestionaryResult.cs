﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Task24_Low.Models
{
    public class QuestionaryResult:BaseEntity
    {
        public int QuestionId { get; set; }
        public string Answer { get; set; }
    }
}