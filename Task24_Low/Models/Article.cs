﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Task24_Low.Models
{
    public class Article : BaseEntity
    {
        public string Title { get; set; }
        public string Content { get; set; }
        public DateTime DatePosted { get; set; }
    }

}